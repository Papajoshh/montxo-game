﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour
{
    [SerializeField]
    int x, y;
    [SerializeField]
    CharacterController controller;

    public void Move()
    {
        if (controller == null && GameManager.instance?.instantiatedCharacterControllers.Count > 0)
        {
            controller = GameManager.instance?.instantiatedCharacterControllers[0];
        }
        controller.MoveTo(y, x);
    }
}
