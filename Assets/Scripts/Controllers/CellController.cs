﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellController : MonoBehaviour
{
    [SerializeField]
    TextMesh debugText;
    public int row;
    public int column;

    public CharacterController associatedCharacterController;
    const string debugString = "{0}-{1}";

    public const string layerMaskName = "SnapHexGrid";
    public void AssociateCharacterController(CharacterController _characterController)
    {
        associatedCharacterController = _characterController;
        associatedCharacterController.AssociateCellController(this);
    }

    public void PrepareCell(int _row, int _column)
    {
        row = _row;
        column = _column;
        if (GameManager.instance.debug)
        {
            debugText.text = string.Format(debugString, row, column);
        }
    }

    private void OnMouseEnter()
    {
        GameManager.actualCell = this;
    }

    public bool IsEmpty()
    {
        return associatedCharacterController == null;
    }

    public void ClearCell()
    {
        if (associatedCharacterController != null)
        {
            associatedCharacterController.ClearData();
        }
        associatedCharacterController = null;
    }
}
