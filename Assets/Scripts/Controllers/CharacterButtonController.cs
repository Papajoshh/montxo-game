﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharacterButtonController : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    CharacterData character;
    [SerializeField]
    Image characterImage;


    public void LoadInfo()
    {
        characterImage.sprite = character.sprite;
    }

    public void SetCursor()
    {
        UIController.instance.SetCursor(character, Input.mousePosition);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        GameManager.actualCell = null;
        GameManager.actualCharacterData = null;
        SetCursor();
    }
}
