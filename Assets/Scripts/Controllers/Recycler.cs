﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recycler : MonoBehaviour
{
    public static bool onRecycler
    {
        get
        {
            return GameManager.actualCell == null;
        }
    }
}
