﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharacterController : MonoBehaviour
{

    private CharacterData characterStored;

    public CellController associatedCellController;

    private CellController m_associatedCellController;
    [SerializeField]
    CharacterAnimationController characterAnimationController;

    [SerializeField]
    float angleToAdd = 0.1f;
    [SerializeField]
    AnimationCurve animationTransition;
    Vector3 initialCellPosition;
    Vector3 endCellPosition;
    Vector3 positionTo;
    CellController endCellController;
    CellController targetCellController;
    CharacterController targetCharacterController;

    Ray ray;
    RaycastHit hit;
    int auxiliarMove = 0;
    bool canDrag;
    bool canRotate;
    bool move;
    bool clickRight;
    bool clickLeft;
    float zPosition
    {
        get
        {
            if (!m_zInitialized)
            {
                m_zPosition = (Director.mainCamera.WorldToScreenPoint(transform.position)).z;
                m_zInitialized = true;
            }
            return m_zPosition;
        }
    }
    float m_zPosition;
    bool m_zInitialized;

    Vector2 endCell;

    float prog = -1;
    float lastRightClick;
    float differenceRightClick;

    [HideInInspector]
    public float actualLife;
    Plane movementPlane;

    void Awake()
    {
        movementPlane = new Plane(Vector3.up, new Vector3(0, GameManager.instance.boardContainer.position.y + 0.1f, 0));
    }

    public void PrepareController(CharacterData _characterData)
    {
        characterStored = _characterData;
        characterAnimationController.SetVisual(_characterData, this);
    }

    public void AssociateAnimator(Animator _animator)
    {
        characterAnimationController.animator = _animator;
    }

    void Update()
    {
        if (Input.GetMouseButton(1) && clickRight)
        {
            differenceRightClick = Input.mousePosition.x - lastRightClick;
            lastRightClick = Input.mousePosition.x;
        }
        if (Input.GetMouseButtonUp(1) && clickRight)
        {
            canRotate = false;
            clickRight = false;
        }
        if (canDrag)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float distance;
            if (movementPlane.Raycast(ray, out distance))
            {
                transform.position = ray.GetPoint(distance);
            }
        }
        if (canRotate)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y - differenceRightClick * angleToAdd, transform.eulerAngles.z);
        }
        if (move)
        {
            if (prog >= 0)
            {
                prog = Mathf.Clamp01(prog + Time.deltaTime / GameManager.instance.gameConfig.turnDuration);
                transform.position = Vector3.Lerp(initialCellPosition, endCellPosition, animationTransition.Evaluate(prog));
                characterAnimationController.ChangeVel(0.6f);
                if (prog >= 1)
                {
                    associatedCellController.ClearCell();
                    associatedCellController = endCellController;
                    move = false;
                    characterAnimationController.ChangeVel(0);
                }
            }
        }
    }

    public void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            clickRight = true;
            canRotate = true;
            lastRightClick = Input.mousePosition.x;
        }
    }

    public void OnMouseDown()
    {
        clickLeft = true;
        if (!canRotate)
        {
            canDrag = true;
        }
        move = false;
    }

    public void OnMouseUp()
    {
        clickLeft = false;
        canDrag = false;
        canRotate = false;
        if (Recycler.onRecycler)
        {
            RecycleCharacter();
            return;
        }

        if (GameManager.actualCell.IsEmpty())
        {
            associatedCellController.ClearCell();
            GameManager.instance.SetCharacterControllerOnCell(GameManager.actualCell, this);
        }
        else
        {
            GameManager.instance.SetCharacterControllerOnCell(associatedCellController, this);
        }
    }

    public void AssociateCellController(CellController _cellController)
    {
        associatedCellController = _cellController;
    }

    public void RecycleCharacter()
    {
        GameManager.instance.ClearCharacterController(this);
        associatedCellController.ClearCell();
    }

    public void ClearData()
    {
        associatedCellController = null;
    }

    public void MoveTo(int _column, int _row)
    {
        
        if (!move)
        {
            endCell = new Vector2(_column, _row);
            move = true;
            initialCellPosition = new Vector3(associatedCellController.transform.position.x, transform.position.y ,associatedCellController.transform.position.z);
            if (_row == 0)
            {
                auxiliarMove = _column;
            }
            else
            {
                if (_column > 0)
                {
                    auxiliarMove = (associatedCellController.row % 2 == 0) ? (_column - 1) : _column;
                }
                else
                {
                    auxiliarMove = (associatedCellController.row % 2 == 1) ? (_column + 1) : _column;
                }
            }
            
            endCellController = GameManager.instance.GetCellController(associatedCellController.row + _row, associatedCellController.column + auxiliarMove);
            endCellPosition = new Vector3(endCellController.transform.position.x, transform.position.y ,endCellController.transform.position.z);
            prog = 0;
        }
    }

    public void ActionTurn()
    {
        if (targetCellController!= null && GameManager.instance.GetDistance(associatedCellController, targetCellController) > characterStored.range || targetCellController == null)
        {
            GoToNearCharacter();
        }
        else if (targetCharacterController != null)
        {
            Attack(targetCharacterController);
        }
    }

    public void GoToNearCharacter()
    {
        targetCharacterController = GameManager.instance.GetNearCharacterController(this);
        targetCellController = targetCharacterController.associatedCellController;

        Vector2 distance = new Vector2(targetCellController.column - associatedCellController.column, targetCellController.row - associatedCellController.row);

        if (GameManager.instance.GetDistance(associatedCellController, targetCellController) > characterStored.range)
        {
            MoveTo((int)Mathf.Clamp(distance.x, -1, 1), (int)Mathf.Clamp(distance.y, -1, 1));
        }
    }

    public float GetActualLife()
    {
        return actualLife;
    }

    public void ChangeLife(float _value)
    {
        actualLife += _value;
    }

    public void ReceiveDamage(float _value)
    {
        ChangeLife(-_value);
        if (GetActualLife() <= 0)
        {
            RecycleCharacter();
        }
    }

    public void ReceiveHeal(float _value)
    {
        ChangeLife(+_value);
    }

    public void Attack(CharacterController _targetController)
    {
        _targetController.ReceiveDamage(characterStored.GetAttackDamage());
        characterAnimationController.DoAttack();
    }
}
    
