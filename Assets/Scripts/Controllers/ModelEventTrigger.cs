﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelEventTrigger : MonoBehaviour
{
    public CharacterController associatedCharacterController;
    [SerializeField]
    Animator animator;

    public void AssociatCharacterController(CharacterController _characterController)
    {
        associatedCharacterController = _characterController;
        associatedCharacterController.AssociateAnimator(animator);
    }

    private void OnMouseDown()
    {
        associatedCharacterController?.OnMouseDown();
    }

    private void OnMouseUp()
    {
        associatedCharacterController?.OnMouseUp();
    }

    private void OnMouseOver()
    {
        associatedCharacterController?.OnMouseOver();
    }
    
}
