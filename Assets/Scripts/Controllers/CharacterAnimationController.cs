﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CharacterAnimationController : MonoBehaviour
{
    [SerializeField]
    Transform characterContainer;
    [SerializeField]
    public Animator animator;

    public void SetVisual(CharacterData _characterData, CharacterController _characterController)
    {
        GameObject instantiatedModel = Instantiate(_characterData.modelPrefab);
        instantiatedModel.GetComponent<ModelEventTrigger>().AssociatCharacterController(_characterController);
        instantiatedModel.transform.localScale = Vector3.one;
        instantiatedModel.transform.SetParent(characterContainer, false);
        instantiatedModel.transform.localPosition = Vector3.zero;
    }

    public void ChangeVel(float _param)
    {
        animator.SetFloat("Vel", _param);
    }

    public void DoAttack()
    {
        animator.Play(Animator.StringToHash("Attack"));
    }
}
