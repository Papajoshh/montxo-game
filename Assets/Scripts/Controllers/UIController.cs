﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    Transform cursorContainer;

    public Transform trashPool;
    public static UIController instance;

    float distance;
    bool characterCursorActive;
    Vector3 position;
    Plane movementPlane;
    Ray ray;
    RaycastHit hit;
    GameObject instantiatedModel;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        
    }

    private void Start()
    {
        movementPlane = new Plane(Vector3.up, new Vector3(0, GameManager.instance.boardContainer.position.y + 0.1f, 0));
    }
        
    private void Update()
    {
        if (characterCursorActive)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            distance = 0;
            if (movementPlane.Raycast(ray, out distance))
            {
                cursorContainer.position = ray.GetPoint(distance);
            }
            if (Input.GetMouseButtonUp(0))
            {
                cursorContainer.gameObject.SetActive(false);
                characterCursorActive = false;
                Destroy(instantiatedModel);
                if (GameManager.IsCellSelected())
                {
                    GameManager.instance.InstantiateCharacterOnActualCell();
                }
            }
        }
    }

    public void SetCursor(CharacterData _characterData, Vector2 _position)
    {
        cursorContainer.gameObject.SetActive(true);
        instantiatedModel = Instantiate(_characterData.modelPrefab);
        instantiatedModel.transform.SetParent(cursorContainer, false);
        instantiatedModel.transform.localPosition = Vector3.zero;
        characterCursorActive = true;
        GameManager.actualCharacterData = _characterData;
    }
}

