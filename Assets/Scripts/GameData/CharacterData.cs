﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterData", menuName = "Data/Create CharacterData")]
public class CharacterData : ScriptableObject
{
    public Sprite sprite;
    public GameObject modelPrefab;
    public int healthPoints;
    public int attackDamage;
    public float attackSpeed;
    public int defense;
    public int magicDefense;
    public float range;
    public float resource;
    public float recourceCreateRate;

    public int GetAttackDamage()
    {
        return GetBaseAttackDamage();
    }

    public int GetBaseAttackDamage()
    {
        return attackDamage;
    }
}
