﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Director : MonoBehaviour
{
    public static Camera mainCamera
    {
        get
        {
            if (m_mainCamera == null)
            {
                m_mainCamera = Camera.main;
            }
            return m_mainCamera;
        }
    }

    static Camera m_mainCamera;

    public static Director instance;

    private void Awake()
    {
        instance = this;
    }
}
    

