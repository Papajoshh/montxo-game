﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEditor : MonoBehaviour
{
    [SerializeField]
    CharacterButtonController[] characterButtons;

    // Start is called before the first frame update
    void Start()
    {
        for (int index = 0; index < characterButtons.Length; index++)
        {
            characterButtons[index].LoadInfo();
        }
    }
}
