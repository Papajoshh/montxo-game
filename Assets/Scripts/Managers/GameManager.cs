﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool debug;
    public GameConfig gameConfig;
    public float cellWidth, cellHeight;
    [SerializeField]
    GameObject cellPrefab;
    [SerializeField]
    GameObject characterControllerPrefab;
    public Transform boardContainer;
    [SerializeField]
    Transform characterContainer;
    [SerializeField]
    HexTileGrid.HexTileGrid hexGrid;

    public static GameManager instance;
    public static CellController actualCell;
    public static CharacterData actualCharacterData;

    CharacterController auxCharacterController;

    public List<CellController> instantiatedCellControllers = new List<CellController>();
    public List<CharacterController> instantiatedCharacterControllers = new List<CharacterController>();
    List<CharacterController> freeCharacterControllers = new List<CharacterController>();
    public List<CharacterController> inUseCharacterControllers = new List<CharacterController>();

    Dictionary<Vector2, CellController> positionToCellController = new Dictionary<Vector2, CellController>();
    Dictionary<CellController, CharacterController> cellToCharacterController = new Dictionary<CellController, CharacterController>();

    float minCellDistance;
    float progTurn = -1;
    private float innerCellRadius;
    Ray ray;
    RaycastHit hit;
    Vector2 mouseInput;

    private void Awake()
    {
        instance = this;
    }

    public void LoadBoard()
    {
        ClearData();
        progTurn = -1;
        positionToCellController.Clear();
        hexGrid.m_gridWidth = gameConfig.columns;
        hexGrid.m_gridHeight = gameConfig.rows;
        innerCellRadius = hexGrid.GetInnerRadius() * 2;
        while (instantiatedCellControllers.Count < gameConfig.rows * gameConfig.columns)
        {
            CellController cellController = Instantiate(cellPrefab).GetComponent<CellController>();
            instantiatedCellControllers.Add(cellController);
        }

        for (int rowIndex = 0; rowIndex < gameConfig.rows; rowIndex++)
        {
            for (int columnIndex = 0; columnIndex < gameConfig.columns; columnIndex++)
            {
                instantiatedCellControllers[rowIndex * gameConfig.columns + columnIndex].PrepareCell(rowIndex, columnIndex);
                instantiatedCellControllers[rowIndex * gameConfig.columns + columnIndex].transform.SetParent(boardContainer, false);
                instantiatedCellControllers[rowIndex * gameConfig.columns + columnIndex].transform.localPosition = new Vector3(columnIndex * innerCellRadius + (rowIndex%2 * innerCellRadius / 2),0, +rowIndex * innerCellRadius);
                positionToCellController.Add(new Vector2(rowIndex, columnIndex), instantiatedCellControllers[rowIndex * gameConfig.columns + columnIndex]);
            }
        }

        for (int index = gameConfig.rows * gameConfig.columns; index < instantiatedCellControllers.Count; index++)
        {
            instantiatedCellControllers[index].transform.SetParent(UIController.instance.trashPool);
            instantiatedCellControllers[index].transform.localPosition = Vector3.zero;
        }
    }

    public void StartGame()
    {
        RunTurn();
    }

    void RunTurn()
    {
        progTurn = 0;
        for (int index = 0; index < inUseCharacterControllers.Count; index++)
        {
            inUseCharacterControllers[index].ActionTurn();
        }
    }

    private void Update()
    {
        if (progTurn >= 0)
        {
            UpdateTurn();
        }
        mouseInput = Input.mousePosition;
    }

    private void FixedUpdate()
    {
        ray = Camera.main.ScreenPointToRay(mouseInput);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer(CellController.layerMaskName)))
        {
            GameManager.actualCell = hit.collider.gameObject.GetComponentInParent<CellController>();
        }
        else
        {
            GameManager.actualCell = null;
        }
    }

    void UpdateTurn()
    {
        if (progTurn >= 0)
        {
            progTurn = Mathf.Clamp01(progTurn + Time.deltaTime / (gameConfig.turnDuration));
            if (progTurn >= 1)
            {
                RunTurn();
            }
        }
    }

    public void InstantiateCharacterOnActualCell()
    {
        if (actualCell.IsEmpty())
        {
            InstantiateCharacterControllerOnCell(actualCell, actualCharacterData);
        }
    }

    public void InstantiateCharacterControllerOnCell(CellController _cellController, CharacterData _characterData)
    {
        CharacterController controller = GetCharacterController(_characterData);
        inUseCharacterControllers.Add(controller);
        SetCharacterControllerOnCell(_cellController, controller);
    }

    public CellController GetCellController(int _row, int _column)
    {
        return positionToCellController[new Vector2(_row, _column)];
    }

    public void SetCharacterControllerOnCell(CellController _cellController, CharacterController _characterController)
    {
        _characterController.transform.SetParent(characterContainer, false);
        _characterController.transform.position = _cellController.transform.position;
        _cellController.AssociateCharacterController(_characterController);
    }
    
    public CharacterController GetCharacterController(CharacterData _characterData)
    {
        if (freeCharacterControllers.Count > 0)
        {
            auxCharacterController = freeCharacterControllers[0];
            freeCharacterControllers.RemoveAt(0);
        }
        else
        {
            CharacterController instantiatedCharacterController = Instantiate(characterControllerPrefab).GetComponent<CharacterController>();
            instantiatedCharacterControllers.Add(instantiatedCharacterController);
            auxCharacterController = instantiatedCharacterController;
        }
        if (!auxCharacterController.gameObject.activeSelf)
        {
            auxCharacterController.gameObject.SetActive(true);
        }
        auxCharacterController.PrepareController(_characterData);
        return auxCharacterController;
    }

    public void ClearCharacterController(CharacterController _controller)
    {
        if (inUseCharacterControllers.Contains(_controller))
        {
            int index = inUseCharacterControllers.IndexOf(_controller);
            freeCharacterControllers.Add(_controller);
            inUseCharacterControllers[index].transform.SetParent(UIController.instance.trashPool, false);
            inUseCharacterControllers[index].transform.localPosition = Vector3.zero;
            inUseCharacterControllers[index].gameObject.SetActive(false);
            inUseCharacterControllers.Remove(_controller);
        }
    }

    void ClearAllCharacters()
    {
        for (int index = inUseCharacterControllers.Count -1; index >= 0; index--)
        {
            freeCharacterControllers.Add(inUseCharacterControllers[index]);
            inUseCharacterControllers[index].transform.SetParent(UIController.instance.trashPool, false);
            inUseCharacterControllers[index].transform.localPosition = Vector3.zero;
            inUseCharacterControllers[index].gameObject.SetActive(false);
        }
        inUseCharacterControllers.Clear();
    }

    void ClearCellInfo()
    {
        for (int index = 0; index < instantiatedCellControllers.Count; index++)
        {
            instantiatedCellControllers[index].ClearCell();
        }
    }

    public void ClearData()
    {
        ClearCellInfo();
        ClearAllCharacters();
    }

    public static bool IsCellSelected()
    {
        return actualCell != null;
    }

    public CharacterController GetNearCharacterController(CharacterController _controller)
    {
        CharacterController nearCharacterController = inUseCharacterControllers[0];
        minCellDistance = float.MaxValue;
        for (int index = 0; index < inUseCharacterControllers.Count; index++)
        {
            if (Vector2.Distance(inUseCharacterControllers[index].transform.position, _controller.transform.position) < minCellDistance && _controller != inUseCharacterControllers[index])
            {
                minCellDistance = Vector2.Distance(inUseCharacterControllers[index].transform.position, _controller.transform.position);
                nearCharacterController = inUseCharacterControllers[index];
            }
        }
        return nearCharacterController;
    }

    public int GetDistance(CellController a, CellController b)
    {
        return CubeDistance(OffsetToCube(a), OffsetToCube(b));
    }

    Hex OffsetToCube(CellController a)
    {
        int q = a.column - (int)((a.row - 1 * (a.row & 1)) / 2);
        int s = a.row ;
        int r = -q - s;
        return new Hex(q, r, s);
    }

    int CubeDistance(Hex a, Hex b)
    {
        return (Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z)) / 2;
    }

}

public struct Hex
{
    public int x, y, z;

    public Hex(int _q, int _r, int _s)
    {
        x = _q;
        y = _r;
        z = _s;
    }
}

[System.Serializable]
public struct GameConfig
{
    public int rows;
    public int columns;
    public float turnDuration;
}
